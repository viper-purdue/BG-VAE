## Theoretical Bound-Guided Hierarchical VAE for Neural Image Codecs. 

This is the pytorch implementation of the paper "Theoretical Bound-Guided Hierarchical VAE for Neural Image Codecs", which is accepted by ICME2024.


[ArXiv](https://arxiv.org/abs/2403.18535) 


### Environment
```
conda create -n VAE python=3.8
conda activate VAE
pip install -U pip && pip install -e .
pip install -r requirements.txt
```
You also need to install the [Pytorch](https://pytorch.org/get-started/locally/) and [Compressai](https://github.com/InterDigitalInc/CompressAI) based on your machine.

### Dataset
#### COCO
1. Download the [COCO 2017 Train Images](http://images.cocodataset.org/zips/train2017.zip). Could be downloaded by running the following command if you are using Linux:
```
wget http://images.cocodataset.org/zips/train2017.zip
```
2. Modify the path in the `VAE/paths.py` file to the path of the COCO dataset.

#### Kodak, Tecnick, and CLIC-2022-Test
```
python scripts/download-dataset.py --name kodak         --datasets_root /path/to/datasets
                                          clic2022-test
                                          tecnick
```
### Results
Kodak: ```results/kodak/kodak-BG-VAE.json```

Tecnick: ```results/tecnick/tecnick-BG-VAE.json```

CLIC-2022-Test: ```results/clic2022-test/clic2022-test-BG-VAE.json```


### Training
Step1: Train the theoretical-bound model.
```
train-var-rate.py --model B_VAE --batch_size 32 --iterations 2000000 --name B_VAE 
```
Step2: Train the practical model by learning from theoretical-bound model. Remember to modify the ` cfg1['pretrained']` to the path of the pretrained theoretical-bound model.
```
train-var-rate.py --model BG_VAE --batch_size 32 --iterations 2000000 --name BG_VAE
```
You can also enable "mixed precision training" and "torch.compile" by adding `--amp` and `--compile` respectively.

Multi-GPU training is supported by:
```
CUDA_VISIBLE_DEVICES=GPU_id torchrun --nproc_per_node number_of_used_GPUs train-var-rate.py --xxx
```

### Testing
Evaluate the theoretical-bound model on the Kodak dataset:
```
python VAE/models/rd/evaluate.py --model B_VAE --dataset_name kodak --device cuda:0
```
Evaluate the practical model on the Kodak dataset:
```
python eval-var-rate.py --model BG_VAE --dataset_name kodak --device cuda:0
```

### Contact
If you have any questions, please contact me at zhan5096@purdue.edu.

### Citation


If you find this code useful, please consider citing:

```
@article{zhang2024theoretical,
  title={Theoretical Bound-Guided Hierarchical VAE for Neural Image Codecs},
  author={Zhang, Yichi and Duan, Zhihao and Huang, Yuning and Zhu, Fengqing},
  journal={arXiv preprint arXiv:2403.18535},
  year={2024}
}

@article{duan2023qarv,
  title={Qarv: Quantization-aware resnet vae for lossy image compression},
  author={Duan, Zhihao and Lu, Ming and Ma, Jack and Huang, Yuning and Ma, Zhan and Zhu, Fengqing},
  journal={IEEE Transactions on Pattern Analysis and Machine Intelligence},
  year={2023},
  publisher={IEEE}
}

@inproceedings{duan2023improved,
  title={An Improved Upper Bound on the Rate-Distortion Function of Images},
  author={Duan, Zhihao and Ma, Jack and He, Jiangpeng and Zhu, Fengqing},
  booktitle={2023 IEEE International Conference on Image Processing (ICIP)},
  pages={246--250},
  year={2023},
  organization={IEEE}
}
